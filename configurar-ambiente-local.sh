# Instalar dependências
sudo sh -c "echo \"deb http://ppa.launchpad.net/ansible/ansible/ubuntu $(grep 'UBUNTU_CODENAME=' /etc/os-release | tr '=' '\n'| tail -1)\" main > /etc/apt/sources.list.d/ansible.list"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
#sudo apt-add-repository ppa:ansible/ansible
sudo apt update && sudo apt install -y ansible git curl python3 software-properties-common

ansible-galaxy collection install community.general && sudo ansible-playbook ansible/install.yml -i ansible/inventory
